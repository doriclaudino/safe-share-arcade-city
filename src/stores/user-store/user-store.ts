import { getRoot, types } from 'mobx-state-tree'
import * as actions from './user-actions'
import { DatabaseUser, DatabaseUserModel, FirebaseUser, FirebaseUserModel, SettingsModel } from './user-models'
import firebase from "react-native-firebase"
import { trueDelay } from '../../lib/delay';

/**
 * Handles user state
 */
export const UserStoreModel = types
  .model('UserStore')
  .props({
    // databaseUser: types.maybe(DatabaseUserModel),
    fbToken: types.maybe(types.string),
    firebaseUser: types.maybe(FirebaseUserModel),
    settings: types.optional(SettingsModel, {}),
    invitedBy: types.maybe(types.string)
  })
  .volatile(self => ({
    authStateHandler: null
  }))
  .actions(self => ({
    authStateHandlerAdd: async (): Promise<boolean> =>
      await actions.authStateHandlerAdd(self),
    authStateHandlerRemove: async (): Promise<boolean> =>
      await actions.authStateHandlerRemove(self),
    checkInviteCode: async (code: string): Promise<any> =>
      await actions.checkInviteCode(self, code),
    bioSelected: async (bio: string): Promise<boolean> =>
      await actions.bioSelected(self, bio),
    loginFacebook: async (): Promise<boolean> =>
      await actions.loginFacebook(self),
    logOut: async (): Promise<boolean> =>
      await actions.logOut(self),
    roleSelected: async (role: string): Promise<boolean> =>
      await actions.roleSelected(self, role),
    saveBio: async (bio: string): Promise<boolean> =>
      await actions.saveBio(self, bio),
    setTermsAgree: async (when: Date): Promise<boolean> =>
      await actions.setTermsAgree(self, when),
    usernameSelected: async (username: string): Promise<boolean> =>
      await actions.usernameSelected(self, username),
    setSettings: async (value: any): Promise<boolean> =>
      await actions.setSettings(self, value),
    setPassword: async (value: string): Promise<boolean> => {
      /** TODO REPLACE the trueDelay with PHP backend then delete this commentary
       * return true for sucess otherwise we show an error like "check fields.."
      */
      return trueDelay(2000)
    },
    attemptToSaveInvitedBy: async (value: any): Promise<boolean> =>
      await actions.saveInvitedBy(self, value),
    setInvitedBy(value: string) {
      self.invitedBy = value
    },
    setFbToken(value: string) {
      self.fbToken = value
    },
    setDatabaseUser(value: DatabaseUser) {
      self.databaseUser = value
    },
    setFirebaseUser(value: FirebaseUser) {
      self.firebaseUser = value
    },
    reset() {
      self.databaseUser = undefined
      self.fbToken = undefined
      self.firebaseUser = undefined
      self.invitedBy = undefined
    }
  }))
  .views(self => ({
    get bio() {
      const root: any = getRoot(self)
      const { firebaseUser: { uid } } = self
      const { bio }: any = root.playerStore.players.get(uid)
      return bio
    },
    get name() {
      const { firebaseUser: { name } } = self
      return name
    },
    get photo() {
      const { firebaseUser: { photoURL } } = self
      return photoURL
    },
    get role() {
      const root: any = getRoot(self)
      const { firebaseUser: { uid } } = self
      const { role }: any = root.playerStore.players.get(uid)
      return role
    },
    get uid() {
      const { firebaseUser: { uid } } = self
      return uid
    },
    get isUserLoggedIn() {
      return self.firebaseUser
    }
  }))

/**
 * An instance of a UserStore.
 */
export type UserStore = typeof UserStoreModel.Type

/**
 * The serialized version of a `UserStore` often used when acquiring
 * data from an API (for example).
 */
export type UserStoreSnapshot = typeof UserStoreModel.SnapshotType
