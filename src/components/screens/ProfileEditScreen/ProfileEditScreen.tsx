import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { UserStore } from '../../stores'
import { Container, Text } from '../../'

export interface ProfileEditScreenProps {
  userStore?: UserStore
}

@inject('userStore')
@observer
export class ProfileEditScreen extends React.Component<ProfileEditScreenProps, {}> {
  public render() {
    return (
      <Container>
        <Text style={{color: 'white'}}>ProfileEditScreen!</Text>
      </Container>
    )
  }
}
