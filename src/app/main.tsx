import { AppRegistry, YellowBox } from 'react-native'
import { RootComponent } from './root-component'
import { StatusBar } from 'react-native'

StatusBar.setBarStyle('light-content', true) // https://stackoverflow.com/questions/39297291/how-to-set-ios-status-bar-background-color-in-react-native
import { StorybookUI } from '../../storybook/storybook'

/**
 * this flag control the next app flow
 */
const SHOW_STORYBOOK = false
const TopLevel = SHOW_STORYBOOK && __DEV__ ? StorybookUI : RootComponent

YellowBox.ignoreWarnings([
  "Missing 'module' parameter",
  "Require cycle",
  "Attempted to invoke",
  "Cannot update during an existing state transition",
  "RCTBridge required dispatch_sync to load RCTDevLoadingView",
  "Can't perform a React state",
  "Unknown data type"
])

// App name needs to match what's in app_delegate.m and MainActivity.java
AppRegistry.registerComponent('city', () => TopLevel)
