import * as React from 'react'
import { View, ViewStyle, ActivityIndicator } from 'react-native'
import { NavButton, Screen, Text, Button, TextField, Toast } from '../../../shared'
import { spacing, color } from '../../../theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import { inject, observer } from 'mobx-react';
import { ToastStore, UserStore, NavigationStore } from '../../../../stores';

const CONTAINER: ViewStyle = {
  paddingBottom: spacing[7],
  paddingTop: spacing[5]
}

interface CreatePasswordScreenProps {
  rulesPattern?: Array<{ regexRule: string, errorTx: string }>,
  toastStore?: ToastStore,
  userStore?: UserStore,
  navigationStore?: NavigationStore
}

interface CreatePasswordScreenState {
  password: string,
  confirmPassword: string,
  passwordValidated: boolean,
  showConfirmPasswordText: boolean,
  showPasswordText: boolean,
  confirmPasswordError: string | undefined,
  passwordError: string | undefined,
  busy: boolean,
  passwordConfirmed: boolean
}

const eyeButton: ViewStyle = {
  justifyContent: 'center', alignItems: 'center', height: 38, width: 38, minHeight: 30, minWidth: 30
}
/**
 * 
 * set user new password
 * when success navigate to menu (user profile)
 * 
 * passwordClickInterval === 3000 === toast time
 * 
 * we handle the next->next->next on keyboard, every attempt user wait 3sec (and show a toast)
 */
@inject('toastStore', 'userStore', 'navigationStore')
@observer
export class CreatePasswordScreen extends React.Component<CreatePasswordScreenProps, CreatePasswordScreenState> {
  constructor(props) {
    super(props)

    this.state = {
      ...this.initialState()
    }
    console.log(`locale :${this.props.userStore.settings.locale}`)
  }

  initialState() {
    return {
      passwordValidated: false,
      password: undefined,
      confirmPassword: undefined,
      showConfirmPasswordText: false,
      showPasswordText: false,
      confirmPasswordError: undefined,
      passwordError: undefined,
      busy: false,
      passwordConfirmed: false
    }
  }

  /**
   * here will be the i18n keys
   */
  description = "settings.password.description"
  defaultNotMatchError = "settings.password.defaultNotMatchError"
  createPasswordTextButton = "settings.password.createPasswordTextButton"
  recoveryPasswordText = "settings.password.recoveryPasswordText"

  passwordLabel = "settings.password.passwordLabel"
  passwordPlaceholder = "settings.password.passwordPlaceholder"
  confirmPasswordLabel = "settings.password.confirmPasswordLabel"
  confirmPasswordPlaceholder = "settings.password.confirmPasswordPlaceholder"

  passwordErrorMessage = "settings.password.passwordErrorMessage"
  passwordSuccessMessage = "settings.password.passwordSuccessMessage"


  /**inputs to control next-->next--> keyboard action */
  passwordInput
  confirmPasswordInput
  savePasswordButton
  passwordClickInterval = 3000


  defaultPasswordRules = [
    { regexRule: '^.{8,}$', errorTx: 'settings.password.rule.min8chars' },
    { regexRule: '(?=.*\\d)', errorTx: 'settings.password.rule.oneNumber' },
    { regexRule: '(?=.*[A-Z])', errorTx: 'settings.password.rule.OneUppercaseletter' }]


  /**
   * validate the password and define the message error
   */
  validatePasswordRules(rules, value) {
    //no validations found
    if (!rules) this.setState({ passwordValidated: true, passwordError: undefined });

    if (Array.isArray(rules)) {
      const conditions = rules.map(rule => new RegExp(rule.regexRule, 'g'));
      const booleanArray = conditions.map(condition => condition.test(value));

      /**find the first false, means first rule failed on test */
      const firstErrorIndex = booleanArray.indexOf(false)
      if (firstErrorIndex > -1)
        this.setState({ passwordValidated: false, passwordError: rules[firstErrorIndex].errorTx })
      else
        this.setState({ passwordValidated: true, passwordError: undefined })
    }
  }

  /**
   * compare both passwords and set confirmPassword error based on password
   * the goal is set attention on previous field if still wrong
   */
  handleConfirmPassword() {
    const { passwordValidated, confirmPassword, password } = this.state
    this.setState({ confirmPasswordError: undefined, passwordConfirmed: confirmPassword === password })

    if (confirmPassword === password)
      this.setState({ confirmPasswordError: undefined, passwordConfirmed: true })
    else
      //we dont show confirmPasswordError if password still not valid
      this.setState({ confirmPasswordError: passwordValidated ? this.defaultNotMatchError : undefined, passwordConfirmed: false })
  }

  /** can click on button or go next on keyboard */
  disableButton() {
    const { passwordValidated, passwordConfirmed, busy } = this.state
    return !passwordValidated || !passwordConfirmed || busy
  }

  /**
   * clear the main timeout and set busy = false after 3secs
   */
  releaseButtons() {
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      this.setState({ busy: false })
    }, this.passwordClickInterval);
  }


  /** here we can handle the on press and the next action from keyboard 
   * as well if wants a toast every button click
  */
  timeout
  async handleOnPress() {
    const { busy, password } = this.state
    const { toastStore, userStore: { setPassword } } = this.props
    try {

      /**
       * busy and timeout will handle it
       */
      if (busy)
        throw new Error('User clicking too much')

      this.setState({ busy: true })

      //button is disable but user passthought using keyboard
      if (this.disableButton()) {
        toastStore.showError({ messageTx: this.passwordErrorMessage })
        this.releaseButtons()
      } else {
        //everything ok, we call the backend and wait
        const success = await setPassword(password)

        /**
         * when true
         * show toast and navigate back to menu/userprofile
         */
        if (success) {
          toastStore.showSuccess({ messageTx: this.passwordSuccessMessage });
          this.setState({ ...this.initialState() }, () => {
            this.timeout = setTimeout(() => { this.props.navigationStore.navigateTo('menu') }, this.passwordClickInterval);
          })
        } else {
          toastStore.showError({ messageTx: this.passwordErrorMessage });
          this.releaseButtons()
        }
      }
    } catch (error) {
      toastStore.showError({ messageTx: this.passwordErrorMessage })
      console.log(JSON.stringify(error))
    } finally {
      console.log(`finally`)
      //unfreeze the screen for some reason?
    }
  }

  render() {
    const { rulesPattern = this.defaultPasswordRules } = this.props
    const { showPasswordText, password, passwordError, confirmPassword, showConfirmPasswordText, confirmPasswordError, busy } = this.state

    return (
      <Screen preset="scrollStack">
        <View style={CONTAINER}>
          <NavButton disabled={this.state.busy} onPress={() => this.props.navigationStore.goBack()} />
          <TextField
            //I don't like the 'blink' made by error text
            style={{ minHeight: 112 }}
            disabled={busy}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              this.validatePasswordRules(rulesPattern, this.state.password)
              this.confirmPasswordInput.focus()
            }}
            ref={(ref) => { this.passwordInput = ref; }}
            onBlur={() => {
              this.validatePasswordRules(rulesPattern, this.state.password)
            }}
            autoCorrect={false}
            secureTextEntry={!showPasswordText}
            labelTx={this.passwordLabel}
            placeholderTx={this.passwordPlaceholder}
            value={password}
            errorTx={passwordError}
            onChangeText={value => {
              this.setState({ password: value }, () => { this.validatePasswordRules(rulesPattern, value) })
            }}
            renderRight={
              <Button
                preset={"icon"}
                style={eyeButton}
                onPressIn={() => this.setState({ showPasswordText: true })}
                onPressOut={() => this.setState({ showPasswordText: false })}
              >
                <Icon
                  name="eye"
                  size={18}
                  color={color.dim} />
              </Button>}
          />

          <TextField
            //I don't like the 'blink' made by error text
            style={{ minHeight: 112 }}
            disabled={busy}
            ref={(ref) => { this.confirmPasswordInput = ref; }}
            onSubmitEditing={() => {
              this.handleConfirmPassword()
              this.handleOnPress()
            }}
            onBlur={() => {
              this.handleConfirmPassword()
            }}
            blurOnSubmit={true}
            autoCorrect={false}
            secureTextEntry={!showConfirmPasswordText}
            labelTx={this.confirmPasswordLabel}
            placeholderTx={this.confirmPasswordPlaceholder}
            errorTx={confirmPasswordError}
            value={confirmPassword}
            onChangeText={value => {
              this.setState({ confirmPassword: value }, () => { this.handleConfirmPassword() });
            }}
            renderRight={
              <Button
                preset={"icon"}
                style={eyeButton}
                onPressIn={() => this.setState({ showConfirmPasswordText: true })}
                onPressOut={() => this.setState({ showConfirmPasswordText: false })}
              >
                <Icon
                  name="eye"
                  size={18}
                  color={color.dim} />
              </Button>}
          />
          {rulesPattern && <Text preset="description" tx={this.description} />}
          <Button
            tx={this.createPasswordTextButton}
            preset="primary"
            disabled={this.disableButton()}
            onPress={() => {
              this.handleOnPress()
            }}
          >
            {busy && <ActivityIndicator size="small" color={color.secondary} />}
          </Button>


          <Toast
            toastStore={this.props.toastStore}
          />
        </View>
      </Screen >
    )
  }
}
