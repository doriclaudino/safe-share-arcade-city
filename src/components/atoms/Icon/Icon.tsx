import * as React from 'react'
import { View } from 'react-native'
import Svg, { Rect, Circle, Path } from 'react-native-svg'
import { svgs } from './Icon.svgs'

export const Icon = (props: any) => {
  const {
    icon
  } = props

  const svgPath = svgs[icon]

  return (
    <View style={{paddingLeft: icon === 'google' ? 20 : 0}}>
      <Svg
        height="70"
        width="70"
        viewBox="0 0 512 512"
      >
          <Path d={svgPath} stroke="white" fill="white" />
      </Svg>
    </View>
  )
}
