import { types } from 'mobx-state-tree'

/**
 * Holds the current toast state to display general messages to the user.
 *
 * The `mobx-state-tree` model used to create `ToastStore` instances
 * as well as including as props in other `mobx-state-tree` models.
 */
export const ToastStoreModel = types
  .model('ToastStore')
  .props({
    /** The icon to display */
    icon: types.optional(types.string, 'alert'),
    /** The message to display */
    message: types.maybe(types.string),
    /** The messageTx to display */
    messageTx: types.maybe(types.string),
    /** The toast preset to use */
    type: types.optional(types.enumeration(['info', 'error', 'highlight']), 'info')
  })
  // setters
  .actions(self => ({
    reset() {
      self.type = 'info'
      self.message = ''
      self.messageTx = ''
      self.icon = ''
    },
    showInfo({ message, messageTx, icon }) {
      self.type = 'info'
      self.icon = icon || 'message'
      self.message = message
      self.messageTx = messageTx
    },
    showError({ message, messageTx, icon }) {
      self.type = 'error'
      self.icon = icon || 'alert'
      self.message = message
      self.messageTx = messageTx
    },
    showSuccess({ message, messageTx, icon }) {
      self.type = 'highlight'
      self.icon = icon || 'alert'
      self.message = message
      self.messageTx = messageTx
    },
    showInfoTx(messageTx: string) {
      self.type = 'info'
      self.icon = 'message'
      self.message = messageTx
      self.messageTx = messageTx
    },
    showErrorTx(messageTx: string) {
<<<<<<< HEAD
      self.type = 'error'
=======
      self.type = 'info'
>>>>>>> d33043f8... new toast functions
      self.icon = 'alert'
      self.message = messageTx
      self.messageTx = messageTx
    },
    showSuccessTx(messageTx: string) {
      self.type = 'highlight'
      self.icon = 'alert'
      self.message = messageTx
      self.messageTx = messageTx
    },
  }))

/**
 * Holds the current toast state to display general messages to the user.
 *
 * An instance of a ToastStore.
 */
export type ToastStore = typeof ToastStoreModel.Type

/**
 * The serialized version of a `ToastStore` often used when acquiring
 * data from an API (for example).
 */
export type ToastStoreSnapshot = typeof ToastStoreModel.SnapshotType
