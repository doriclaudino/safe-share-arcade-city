import * as React from 'react'
import { storiesOf } from '@storybook/react-native'
import { StoryScreen } from '../../../../../storybook/views'
import { CreatePasswordScreen } from './index'
import { State } from 'react-powerplug'
import { ToastStoreModel, UserStoreModel, NavigationStoreModel } from '../../../../stores';
import { trueDelay, falseDelay } from '../../../../lib/delay'
import { Provider } from 'mobx-react';
import { Translate } from '../../../../services';
import * as config from '../../../../app/setup/config';

const rules = [
    { regexRule: '^.{8,}$', errorTx: 'settings.password.rule.min8chars' },
    { regexRule: '(?=.*\\d)', errorTx: 'settings.password.rule.oneNumber' },
    { regexRule: '(?=.*[A-Z])', errorTx: 'settings.password.rule.OneUppercaseletter' }]

const translateService = new Translate(config.translate)


storiesOf('Password Screen', module)
    //.addDecorator(story => <Provider store={userStore}>{story()}</Provider>)
    .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
    .add('Success Create Password EN', () => {
        translateService.setLocale(null);
        return <CreatePasswordScreen
            rulesPattern={rules}
            toastStore={ToastStoreModel.create({})}
        />
    })
    .add('Success Create Password  PT-BR', () => {
        translateService.setLocale('pt-BR');
        return <CreatePasswordScreen
            rulesPattern={rules}
            toastStore={ToastStoreModel.create({})}
            userStore={UserStoreModel.create({})}
            navigationStore={NavigationStoreModel.create({})}
        />
    })
    .add('Success Create Password  PT-BR 8 chars only', () => {
        translateService.setLocale('pt-BR');
        return <CreatePasswordScreen
            rulesPattern={[{ regexRule: '^.{8,}$', errorTx: 'settings.password.rule.min8chars' }]}
            toastStore={ToastStoreModel.create({})}
            userStore={UserStoreModel.create({})}
            navigationStore={NavigationStoreModel.create({})}
        />
    })