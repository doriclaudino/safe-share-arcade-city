import * as React from 'react'
import {
  Animated,
  Image,
  SafeAreaView,
  TextStyle,
  View,
  ViewStyle,
  TouchableWithoutFeedback
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { ToastStore } from '../../../stores'
import { Text } from '../../shared'
import { color, spacing } from '../../theme'

// component properties
export interface ToastProps {
  toastStore?: ToastStore
}

// static styles
const ROOT: ViewStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0
}

const SAFE_AREA_VIEW: ViewStyle = {
  backgroundColor: color.info
}

const ROW: ViewStyle = {
  alignItems: 'center',
  flexDirection: 'row',
  paddingBottom: spacing[4],
  paddingHorizontal: spacing[4],
  paddingTop: spacing[1]
}

const ICON: ViewStyle = {
  alignItems: 'center',
  justifyContent: 'center'
}

const TEXT: TextStyle = {
  paddingLeft: spacing[4]
}

// icon map
const ICONS = {
  alert: require('./alert.png'),
  chat: require('./inbox.png'),
  message: require('./message.png')
}

const Y_OFFSET = -120

/**
 * A toast message component to display dismissible messages at the top of the screen.
 */
@inject('toastStore')
@observer
export class Toast extends React.Component<ToastProps, {}> {
  state = {
    y: new Animated.Value(0)
  }
  timeout
  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  componentWillReact() {
    console.tron.display({
      name: 'Toast ',
      value: this.props,
      preview: 'componentWillReact'
    })
    const { toastStore: { message, messageTx } } = this.props
    const whichMessage = message || messageTx
    if (whichMessage) {
      /**seems when we send the same toast, the componentWillReact not react */
      this.slideDown(this.dismiss(3000))
      // Auto-dismiss after 3 seconds
    } else {
      console.log('TRYINGTHIS!')
      this.slideUp()
    }
  }

  dismiss = (ms = 3000) => {
    const { toastStore: { reset } } = this.props
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.slideUp(reset), ms)
  }

  slideDown = (callback = null) => {
    Animated.spring(this.state.y, {
      toValue: 1,
      overshootClamping: true
    }).start(callback)
  }

  slideUp = (callback = null) => {
    Animated.spring(this.state.y, {
      toValue: 0
    }).start(callback)
  }

  render() {
    const { toastStore: { type, message, messageTx, icon } } = this.props
    const rootStyle = {
      ...ROOT,
      transform: [
        {
          translateY: this.state.y.interpolate({
            inputRange: [0, 1],
            outputRange: [Y_OFFSET, 0]
          })
        }
      ]
    }
    const safeAreaStyle = {
      ...SAFE_AREA_VIEW,
      backgroundColor: color[type]
    }

    return (
      <Animated.View style={rootStyle}>
        <TouchableWithoutFeedback onPress={() => this.dismiss(0)}>
          <SafeAreaView style={safeAreaStyle}>
            <View style={ROW}>
              <View style={ICON}>
                <Image source={ICONS[icon]} />
              </View>
              <Text style={TEXT} preset="label" tx={messageTx} text={message} />
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </Animated.View>
    )
  } // render
} // component
