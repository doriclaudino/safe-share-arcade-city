import * as React from 'react'
import { Animated, Image, ImageStyle, View } from 'react-native'

interface BackgroundState {
  bgAnim: any
}

export class Background extends React.PureComponent<{}, BackgroundState> {
  constructor(props) {
    super(props)

    this.state = {
      bgAnim: new Animated.Value(0)
    }
  }

  componentDidMount() {
    Animated.timing(this.state.bgAnim, {
      duration: 6500,
      toValue: 1
    }).start()
  }

  public render() {
    const {
      children,
      ...rest
    } = this.props

    const BACKGROUND: ImageStyle = {
      flex: 1,
      height: '100%',
      opacity: 1,
      width: '100%'
    }

    const animStyle = {
      flex: 1,
      height: '100%',
      opacity: this.state.bgAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1]
      }),
      position: 'absolute',
      width: '100%',
    }

    return (
      <View {...rest} style={{flex: 1, backgroundColor: '#000'}}>
        <Animated.View style={animStyle}>
          <Image
            style={BACKGROUND}
            source={require('./bg2.png')}
          />
        </Animated.View>
        {children}
      </View>
    )
  }
}
