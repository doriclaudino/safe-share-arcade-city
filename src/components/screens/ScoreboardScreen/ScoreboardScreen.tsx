import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { ScoreStore } from '../../stores'
import { Container, Text } from '../../'

interface ScoreboardScreenProps {
  scoreStore?: ScoreStore
}

@inject('scoreStore')
@observer
export class ScoreboardScreen extends React.Component<ScoreboardScreenProps, {}> {
  public render() {
    const { scores } = this.props.scoreStore
    return (
      <Container>
        <Text style={{color: 'white'}}>ScoreboardScreen!</Text>
      </Container>
    )
  }
}
