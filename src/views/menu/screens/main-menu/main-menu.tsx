import * as React from 'react'
import { View } from 'react-native'
import { inject, observer } from 'mobx-react'
import { MenuButton, Screen, Toast } from '../../../shared'
import { ProfileSummary } from '../../../profile'
import { NavigationStore, UserStore, ToastStore } from '../../../../stores'
import { images, color } from '../../../theme'
import Alert from '../../../shared/alert';
import Icon from 'react-native-vector-icons/FontAwesome5'

interface Props {
  navigationStore?: NavigationStore
  userStore?: UserStore,
  toastStore?: ToastStore
}

@inject('navigationStore', 'userStore', 'toastStore')
@observer
export class MainMenu extends React.Component<Props, {}> {


  //RN : show confirmation alert before logout to application : issue #21
  onClickLogout() {
    const {
      userStore: { logOut }
    } = this.props

    Alert.alert("menu.logout", "menu.sureYouWantToLogout",
      [{
        text: 'common.yes', onPress: () => {
          logOut()
        }
      },
      {
        text: 'common.no'
      }])
  }


  render() {
    const { navigateTo } = this.props.navigationStore
    const { firebaseUser, role, settings: { locale } } = this.props.userStore //
    if (!firebaseUser) {
      return (
        <View />
      )
    }

    const { name, fbid, creationTime } = firebaseUser
    const avatar = 'https://graph.facebook.com/' + fbid + '/picture?type=large'
    const username = null
    const bio = 'placeholder'
    const insertedAt = new Date(parseInt(creationTime)) // TODO
    return (
      <Screen preset="scrollStack">
        <ProfileSummary
          avatar={avatar}
          level={1}
          name={name}
          username={username}
          role={role}
          bio={bio}
          insertedAt={insertedAt}
          locale={locale}
        />
        <MenuButton
          image={images.settings}
          titleTx="menu.settings"
          onPress={() => navigateTo('settings')}
        />
        <MenuButton
          titleTx="menu.createPassword"
          onPress={() => navigateTo('createPassword')}
        >
          <Icon
            name="fingerprint"
            size={18}
            color={color.dim} />
        </MenuButton>
        <MenuButton
          image={images.refer}
          titleTx="menu.connect"
          onPress={() => navigateTo('connect')}
        />
        <MenuButton
          image={images.logout}
          titleTx="menu.logout"
          onPress={this.onClickLogout.bind(this)}
          last
        />
        <Toast toastStore={this.props.toastStore} />
      </Screen>
    )
  }
}



/*
<MenuButton
  image={images.note}
  titleTx="menu.changelog"
  onPress={() => navigateTo('changelog')}
/>
<TesterMenuButtons
  navigateTo={navigateTo}
/>
*/

//         <Button style={PROFILE_BUTTON} tx="menu.viewEditProfile" onPress={() => alert('Placeholder')} />
