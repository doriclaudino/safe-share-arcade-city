import { NavigationActions, StackActions } from 'react-navigation'
import { types } from 'mobx-state-tree'
import { RootNavigator } from '../../navigation/root/root-navigator'
import { NavigationEvents } from './navigation-events'
import { prop } from 'ramda'

const { getStateForAction } = RootNavigator.router

const DEFAULT_STATE = RootNavigator.router.getStateForAction(NavigationActions.init(), null)

/**
 * Finds the current route.
 *
 * @param navState the current nav state
 */
function findRoute(navState) {
  const route = navState.routes[navState.index]
  if (route.routes) {
    return findRoute(route)
  }
  return route
}

/**
 * Tracks the navigation state for `react-navigation` as well as providers
 * the actions for changing that state.
 */
export const NavigationStoreModel = NavigationEvents.named('NavigationStore')
  .props({
    loading: types.optional(types.boolean, false),
    loadingMessage: types.optional(types.string, ''),
    deletedGuild: types.optional(types.string, ''),
    /**
     * the navigation state tree (Frozen here means it is immutable.)
     */
    state: types.optional(types.frozen(), DEFAULT_STATE)
  })
  .views(self => ({
    /**
     * Gets the current route.
     */
    get currentRoute() {
      return findRoute(self.state)
    },
    /**
     * Gets the current route name.
     */
    get currentRouteName() {
      return prop('routeName', findRoute(self.state))
    },
    /**
     * Gets the current route params.
     */
    get currentRouteParams() {
      return prop('params', findRoute(self.state))
    },
    /**
     * Gets the current route key.
     */
    get currentRouteKey() {
      return prop('key', findRoute(self.state))
    },

    get getDeletedGuild() {
      return self.deletedGuild
    }
  }))
  .actions(self => ({
    setLoading(value: boolean, message: string) {
      self.loading = value
      self.loadingMessage = message
    }
  }))
  .actions(self => {
    /**
     * Fires when navigation happens.
     *
     * Our job is to update the state for this new navigation action.
     *
     * @param action The new navigation action to perform
     * @param shouldPush Should we push or replace the whole stack?
     */
    const dispatch = (action: {}, shouldPush: boolean = true) => {
      const prevState = shouldPush ? self.state : null
      // @ts-ignore
      const nextState = getStateForAction(action, prevState) || self.state
      self.state = nextState
      self.fireSubscribers(action, prevState, self.state)
      return true
    }

    const setDeletedGuild = (deletedGuild) => {
      self.deletedGuild = deletedGuild
    }

    /**
     * Resets the navigation back to the start.
     */
    const reset = () => {
      self.state = DEFAULT_STATE
      self.loading = undefined
      self.loadingMessage = undefined
    }

    /**
     * Navigate to another place.
     *
     * @param routeName The route name.
     * @param params The route params.
     */
    const navigateTo = (routeName: string, params = {}) => {
      dispatch(NavigationActions.navigate({ routeName, params }))
    }

    /** Navigate deeper to another screen 
     * 
     * @param path 
     * @param params 
     */
    const navigateDeep = (path: string, params = {}) => {
      const paths = path.split('/')
      console.log(paths)
      if (paths.length) {
        newResetTo(paths[0])
        for (let index = 1; index < paths.length; index++) {
          const path = paths[index];
          dispatch(NavigationActions.navigate({ routeName: path, params }))
        }
      }
    }

    /**
     * Dispatches the back nav action.
     */
    const goBack = () => {
      dispatch(NavigationActions.back())
    }

    /**
     * Dispatches the back nav action from a specific route.
     *
     * @param key The key of the route to go back from (wtf why react-navigation?).
     */
    const backFrom = (key: string) => {
      dispatch(NavigationActions.back({ key }))
    }

    /**
     * Dispatches the setParams nav action to a specific route.
     *
     * @param key The route key.
     * @param params The route params.
     */
    const setParams = (key: string, params = {}) => {
      dispatch(NavigationActions.setParams({ params, key }))
    }

    const newResetTo = (routeName: string, params = {}) => {
      dispatch(
        StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate({ routeName })]
        })
      )
    }


    /**
     * Resets the navigator so this routeName is on top. This will
     * unmount all other screens currently in memory.
     * TODO: With .reset() deprecated in 2.0.0, still need to figure out how to unmount other screens
     *
     * @param routeName The route name.
     */
    const resetTo = (routeName: string, params = {}) => {
      dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: routeName })]
        })
      )
    }

    // make these functions available
    return {
      newResetTo,
      backFrom,
      dispatch,
      goBack,
      navigateTo,
      reset,
      resetTo,
      setParams,
      setDeletedGuild,
      navigateDeep
    }
  })

export type NavigationStore = typeof NavigationStoreModel.Type
