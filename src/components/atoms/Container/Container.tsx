import * as React from 'react'
import { StyleSheet, View } from 'react-native'
import { presets } from './Container.presets'

export const Container = (props: any) => {
  const {
    preset = 'default',
    children,
    style: styleOverride
  } = props

  const presetToUse = presets[preset] || presets.default
  const style = { ...presetToUse, ...styleOverride }

  return (
    <View style={style}>
      {children}
    </View>
  )
}
