import * as React from 'react'
import { Platform, Linking } from 'react-native'
import LaunchImage from 'react-native-splash-screen'
import { Provider } from 'mobx-react'
import { setupEnvironment } from './setup/setup-environment'
import { setupRootStore } from './setup/setup-root-store'
import { BackButtonHandler } from '../navigation/back-button-handler'
import { DEFAULT_NAVIGATION_CONFIG } from '../navigation/navigation-config'
import { RootStore } from '../stores/root-store'
import { CodepushWrapperScreen } from './codepush-wrapper'
import { InitWrapper } from './init-wrapper'
import { SplashNoStore } from '../views/loading'
import { delay } from '../lib/delay'
import { contains } from 'ramda'

enum AuthenticatedLinks {
  'settings' = 'main/menu/settings',
  'change-language' = 'main/menu/settings/selector',
  'create-password' = 'main/menu/createPassword',
  'profile' = 'main/menu'
}

interface State {
  ready: boolean,
  store?: RootStore
}

export class RootComponent extends React.Component<{}, State> {
  /**
   * Initial state.
   */
  state: State = { ready: false }

  /**
   * When the component is mounted.
   * Setup/init everything, then wait another 1 second, to allow enough time for splash screen to show.
   * Eventually make it check time passed, and if it's already been more than ~3 seconds, don't add another second.
   */
  async componentDidMount() {
    const env = await setupEnvironment()
    const store = await setupRootStore(env)

    //here is here we define the initial language based on prefs->device->default
    const lang = env.translate.findBestLanguageAvailable(store.userStore.settings.locale)
    console.tron.log('Setting lang to ', lang)

    this.setState({ ready: true, store }, async () => {
      await this.listenDynamicLinks()
      await store.userStore.setSettings({ locale: lang })
      LaunchImage.hide()
    })
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleOpenURL);
  }

  /** handled the initialURL who intent to open the app
   *  handled subsequent urls (can disable this?)
   */
  async listenDynamicLinks() {
    console.log(`listenDynamicLinks`)
    const initialUrl = await Linking.getInitialURL()
    if (initialUrl) this._handleOpenURL({ url: initialUrl })
    Linking.addEventListener('url', (event) => { this._handleOpenURL(event) });
    return true
  }

  /**
   * check if by ours config we can handled this url
   * check if ours regexRule we can handled the invitedBy or navigate deep
   * we assume the user must be logged on firebase
   * 
   * @param event event contains our url to handle
   */
  async _handleOpenURL(event) {
    try {
      const { userStore, navigationStore } = this.state.store

      const canOpen = await Linking.canOpenURL(event.url)
      if (canOpen && userStore.firebaseUser) {
        /**
         * for more information about this regex: https://regex101.com/r/XSdQJz/5
         */
        const regex = new RegExp(`^https?:\/\/arcade\.city\/app\/i\/([0-9]+)$`, "i"); //case insensitive regex
        const matchUserInvitedBy = event.url.match(regex)
        /**
         * match our regex with /path/id
         */
        if (matchUserInvitedBy) {
          console.log(`user trying to set invitedBy code`)
          const refid = matchUserInvitedBy[1]   //refid or other string

          console.log(`navigatingDeep to: ${AuthenticatedLinks.profile}`)
          navigationStore.navigateDeep(AuthenticatedLinks.profile)
          userStore.attemptToSaveInvitedBy(refid)
        } else {
          /**
         * for more information about this regex: https://regex101.com/r/Y2ATGY/2
         */
          const pathOptions = Object.keys(AuthenticatedLinks).map(key => key).join('|')
          const regex = new RegExp(`^https?:\/\/arcade\.city\/app\/(${pathOptions})$`, "i"); //case insensitive regex
          const matchDirectLink = event.url.match(regex)
          if (matchDirectLink) {            
            const path = matchDirectLink[1]
            console.log(`navigatingDeep to: ${path}`)
            navigationStore.navigateDeep(AuthenticatedLinks[path])
          } else
            //allow on configs but our regex not expect it
            console.log(`cannot handle based regex app url ${event.url}`)
        }
      } else {
        //react native configs can't filter or is outdated
        console.log(`cannot handle app url ${event.url}`)
      }
    } catch (error) {
      console.log(`_handleOpenURL error`)
      console.log(error.code, error.message)
    }
  }

  /**
   * Are we allowed to exit the app?  This is called when the back button
   * is pressed on android.
   *
   * @param routeName The currently active route name.
   */
  canExit(routeName: string) {
    return contains(routeName, DEFAULT_NAVIGATION_CONFIG.exitRoutes)
  }

  render() {
    const { ready, store } = this.state
    if (!ready) {
      return (
        <SplashNoStore />
      )
    }

    const injectableStores = {
      chatStore: store.chatStore,
      // gameStore: store.gameStore,
      guildStore: store.guildStore,
      locationStore: store.locationStore,
      // modalStore: store.modalStore,
      navigationStore: store.navigationStore,
      playerStore: store.playerStore,
      toastStore: store.toastStore,
      uiStore: store.uiStore,
      userStore: store.userStore
    }

    // const componentToRender = Platform.OS === 'android' ? <InitWrapper /> : <CodepushWrapperScreen />
    const componentToRender = <InitWrapper />

    return (
      <Provider {...injectableStores}>
        <BackButtonHandler canExit={this.canExit}>
          {componentToRender}
        </BackButtonHandler>
      </Provider>
    )
  }
}
