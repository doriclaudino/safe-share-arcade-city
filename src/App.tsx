import * as React from 'react'
import { Text } from 'react-native'
import { Provider } from 'mobx-react'
import { setupRootStore } from './setup/setup-root-store'
import { Router } from './Router'

interface AppState {
  ready: boolean
  store?: any
}

class App extends React.Component<{}, AppState> {
  constructor (props) {
    super(props)
    this.state = { ready: false }
  }

  public async componentDidMount() {
    const store = await setupRootStore({})
    this.setState({ ready: true, store })
  }

  public render() {
    const { ready, store } = this.state
    if (!ready) {
      return (
        <Text>Loading</Text>
      )
    }

    const injectableStores = {
      navStore: store.navStore,
      userStore: store.userStore,
    }

    return (
      <Provider {...injectableStores}>
        <Router />
      </Provider>
    )
  }
}

export default App
