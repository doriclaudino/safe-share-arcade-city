import firebase from 'react-native-firebase'
import { getEnv, getRoot } from 'mobx-state-tree'
import { Environment } from '../../app/setup/environment-model'
import { RootStore } from '../root-store'
import { afterLogin } from './afterLogin'
import Alert from '../../views/shared/alert'
import { translate } from '../../i18n'
import { Platform } from "react-native"
import { distanceInWordsToNow, differenceInHours } from '../../lib/locale-date-fns';


export async function setTermsAgree(self: any, when: Date) {
  const env = getEnv(self) as Environment
  const root = getRoot(self) as RootStore
  const { uid }: any = root.userStore
  await env.firebase.setTermsAgree(uid, when)
  return true
}


export async function saveBio(self: any, bio: string) {
  // alert('YO SAVING BIO ' + bio)

  const root = getRoot(self) as RootStore
  const { uid }: any = root.userStore

  root.playerStore.players.get(uid).setBio(bio)

  const env = getEnv(self) as Environment
  await env.firebase.userSetBio(uid, bio)

  // root.playerStore.setSelectedPlayer(uid)
  // root.navigationStore.navigateTo('profile')
  // root.navigationStore.newResetTo('main')
  //RN : we are sending back to the user on guild profile screen : issue #25
  root.navigationStore.goBack()
  Alert.alert(undefined, `profile.bioUpdated`)

  // Update beacon in database
  // Update playerobj in database

  // Update playerobj locally - or have this handled by HMMMM automatish? does that solve the weirdness
  // do it locally in case theres no beacon we wount get the


  return true
}


/**
 * The main login function
 */
export async function loginFacebook(self: any) {
  try {
    const env = getEnv(self) as Environment
    const root = getRoot(self) as RootStore

    root.navigationStore.setLoading(true, translate('enter.loggingIn'))

    // Do Facebook login flow, grab access token
    const { error: loginError, kind, ok: loginOk, token }: any = await env.facebook.login()
    self.setFbToken(token)

    // Handle error; end loading state
    if (!loginOk || kind !== 'login') {
      root.navigationStore.setLoading(false, '')
      if (loginError !== 'login-cancelled') {
        Alert.alert(undefined, loginError)
      }
      return false
    }

    // Use the Facebook access token to auth with Firebase, grab user id
    const { uid }: any = await env.firebase.loginWithFacebook(token)

    //Set shouldReceivePushNotifications true to receive notifications after login application
    if (loginOk) {
      root.userStore.settings.setPushNotificationBadgeCount(0)
      root.userStore.settings.setCurrentDevice(Platform.OS)
      env.firebase.userSetNotification(uid, true)
    } else {
      alert('Error: ' + loginError)
    }


    // See if this player exists
    const { error, ok, player }: any = await env.firebase.fetchPlayer(uid)

    // If not, step through onboarding (which will create a player at the end)
    if (error && error === 'player-doesnt-exist') {
      root.navigationStore.navigateTo('gatekeeper')
      root.navigationStore.setLoading(false, '')
      return true
    }

    // If for some reason we return with not ok or no player, get out of here
    if (!ok || !player) {
      Alert.alert(undefined, `errors.unknown`)
      return false
    }

    // Chheck for user settings and update settings object with it
    const { agreedToTerms, settings }: any = await env.firebase.userFetchData(uid)
    // console.tron.log('agreedToTerms:', agreedToTerms)
    // console.tron.log('what the fuck is this:', typeof agreedToTerms)
    if (typeof agreedToTerms === 'undefined') {
      // alert('what the fuck')
      root.navigationStore.navigateTo('terms')
      root.navigationStore.setLoading(false, '')
      return false
    }
    self.setSettings(settings)

    // Otherwise assume we found a player. First save in store.
    root.playerStore.setPlayer(player)

    // If in a guild, set the ID and join the guild chatroom
    const { guild } = player
    if (guild) {
      root.guildStore.setGuildId(guild)
      root.chatStore.joinGuildChatroom(guild)
    }

    // End loading state and redirect to Map
    root.navigationStore.setLoading(false, '')
    root.navigationStore.newResetTo('main')

    return true
  } catch (e) {
    // alert('Login failed: ' + e.message)

    console.tron.log(e.message)
    const root = getRoot(self) as RootStore
    root.userStore.logOut()
    Alert.alert(undefined, `errors.unknownReopenApp`)
    return false
  }
}



export const checkInviteCode = async (self, code) => {
  return { ok: true }
}

export const bioSelected = async (self, bio) => {
  try {
    const env = getEnv(self) as Environment
    const root = getRoot(self) as RootStore
    const uid = self.uid

    const {
      ok, error
    }: any = await env.firebase.userSetBio(uid, bio)

    if (ok) {
      // alert('Updated')
      root.navigationStore.newResetTo('main')
    } else {
      Alert.alert(undefined, `errors.WhatsGoWrong`, undefined, { messageOptions: { message: error } })
    }

  } catch (e) {
    console.tron.log('ERROR')
    console.tron.log(e.message)
  }
  return true
}

export const roleSelected = async (self, role) => {
  try {
    const env = getEnv(self) as Environment
    const root = getRoot(self) as RootStore
    const uid = self.uid

    const {
      ok, error
    }: any = await env.firebase.userSetRole(uid, role)

    if (ok) {
      root.navigationStore.navigateTo('welcome', { step: 'username' })
    } else {
      Alert.alert(undefined, `errors.WhatsGoWrong`, undefined, { messageOptions: { message: error } })
    }

  } catch (e) {
    console.tron.log('ERROR')
    console.tron.log(e.message)
  }
  return true
}

export const usernameSelected = async (self, username) => {
  try {
    const env = getEnv(self) as Environment
    const root = getRoot(self) as RootStore
    const uid = self.uid

    const {
      ok, error
    }: any = await env.firebase.userSetUsername(uid, username)

    if (ok) {
      // alert('Updated')
      root.navigationStore.navigateTo('welcome', { step: 'bio' })
    } else {
      Alert.alert(undefined, `errors.WhatsGoWrong`, undefined, { messageOptions: { message: error } })
    }

  } catch (e) {
    console.tron.log('ERROR')
    console.tron.log(e.message)
  }
  return true
}

export async function authStateHandlerAdd(self) {
  self.authStateHandler = firebase.auth().onAuthStateChanged(user => {
    console.tron.display({
      name: 'Firebase authState changed',
      value: user,
      preview: user ? user.displayName + ' logged in' : 'Logged out'
    })
    const root = getRoot(self) as RootStore
    if (!user) {  // Or no databaseUser AND not fetching databaseUser ?
      root.reset()
      console.tron.clear()
      console.tron.log('No Firebase user, so we reset all stores.')
    } else {
      let modifiedEmail: string

      if (!user.email) {
        modifiedEmail = ''
      } else {
        modifiedEmail = user.email
      }

      // @ts-ignore
      root.userStore.setFirebaseUser({
        email: modifiedEmail,
        uid: user.uid,
        name: user.displayName,
        photoURL: user.photoURL,
        fbid: user.providerData[0].uid,
        creationTime: ""+user.metadata.creationTime,
        lastSignInTime: ""+user.metadata.lastSignInTime
      })
      afterLogin(self, user.uid)

      // For now, lets log out
      // firebase.auth().signOut()
    }
  })
  return true
}

export async function authStateHandlerRemove(self) {
  if (self.unsubscribeFromAuth) {
    console.tron.log('unsubscribing...')
    self.authStateHandler()
    self.authStateHandler = null
  } else {
    console.tron.log('not unsubscribing.....?')
  }
  return true
}

export async function logOut(self) {
  const env = getEnv(self) as Environment
  const uid = self.uid
  // The Firebase logout handler will initiate any other cleanup
  const {
    ok, error
  }: any = await env.firebase.logOut()
  if (ok) {

    env.facebook.logout()
    //Set shouldReceivePushNotifications false to not receive notifications after logout application
    env.firebase.userSetNotification(uid, false)
  } else {
    alert('Error: ' + error)
  }
  return true
}

/**
 *
 * @param self settings ref
 * @param value new settings
 */
export async function setSettings(self, value) {
  if(value['locale'] ){
    const env = getEnv(self) as Environment
    env.translate.setLocale(value['locale'])
  }
  self.settings = {...self.settings, ...value}
  return true
}

/**
 * 
 * @param self 
 * @param value invited code
 */
export async function saveInvitedBy(self, value) {
  try {
    const env = getEnv(self) as Environment
    const root = getRoot(self) as RootStore
    const uid = self.uid

    /**
     * 24h to set the invitedBy
     * means old users can't receive code
     */
    const userCreationTime = new Date(parseInt(self.firebaseUser.creationTime))
    const hoursDiff = differenceInHours(new Date(), userCreationTime)
    const allowHours = 24 * 30 * 12 * 4 //increase to 4 years
    const expireTime = hoursDiff > allowHours

    if (expireTime) {
      console.log(`Account ${uid} was created ${distanceInWordsToNow(userCreationTime, { includeSeconds: true, addSuffix: true })}, unfortunately we expire the link after ${allowHours} hours.`)
      throw new Error(`profile.invitedBy.errorExpire`)
    }

    const { invitedBy }: any = await env.firebase.userFetchData(uid)
    if (invitedBy) {
      if (invitedBy === value) {
        throw new Error(`profile.invitedBy.errorAlready`)
      }
      else {
        throw new Error(`profile.invitedBy.errorAlreadyAndReplace`)
      }
    }

    const saved = await env.firebase.userSetInvitedBy(uid, value)
    if (saved && saved.ok) {
      console.log(`saveInvitedBy success! user ${uid} has invitedBy ${saved['invitedBy']}`)
      root.toastStore.showSuccessTx('profile.invitedBy.success')
      self.setInvitedBy(saved['invitedBy'])
      return true
    }

    throw new Error(`profile.invitedBy.errorRandom`)
  } catch (error) {
    try {
      const root = getRoot(self) as RootStore
      root.toastStore.showErrorTx(error.message)
    } catch (error) {
      console.log(error.code, error.message)
    }
    return false
  }
}
