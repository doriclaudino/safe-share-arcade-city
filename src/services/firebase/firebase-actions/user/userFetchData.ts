import firebase from 'react-native-firebase'
import { capitalize } from '../../../../lib/util'

/**
 * Attempt to fetch user data from Firestore.
 * Add a welcomed boolean for showed welcome piece?
 */

export const userFetchData = async uid => {
  try {
    const userRef = firebase.firestore().collection('users').doc(uid)

    // @ts-ignore
    return userRef.get().then(doc => {
      if (doc.exists) {
        const { agreedToTerms, bio, guild, role, settings, username, invitedBy }: any = doc.data()
        return {
          ok: true,
          kind: 'fetch-user',
          agreedToTerms,
          bio,
          guild,
          role: capitalize(role),
          settings,
          username,
          invitedBy
        }
      } else {
        return { ok: false, error: 'user-doesnt-exist' }
      }
    }).catch(e => {
      console.tron.display({
        name: 'userFetchData firebase error',
        preview: e.message,
        value: e
      })
      return { ok: false, error: 'unknown-error' }
    })

  } catch (e) {
    console.tron.display({
      name: 'userFetchData error',
      preview: e.message,
      value: e
    })
    return { ok: false, error: 'unknown-error' }
  }
}
