import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { UserStore } from '../../stores'
import { Container, Text } from '../../'

export interface ProfileScreenProps {
  userStore?: UserStore
}

@inject('userStore')
@observer
export class ProfileScreen extends React.Component<ProfileScreenProps, {}> {
  public render() {
    return (
      <Container>
        <Text style={{color: 'white'}}>ProfileScreen!</Text>
      </Container>
    )
  }
}
