import * as React from 'react'
import { Text } from 'react-native'
import { inject, observer } from 'mobx-react'
import { NavStore } from './stores'
import { LoginScreen } from './components/screens'

interface RouterProps {
  navStore?: NavStore
}

@inject('navStore')
@observer
export class Router extends React.Component<RouterProps, {}> {
  public render() {
    let screenComponent
    const { screen } = this.props.navStore

    switch (screen) {
      // case 'seedShow':
      //   screenComponent = <Text>Test</Text>
      //   break
      default:
        screenComponent = <LoginScreen />
    }

    return screenComponent
  }
}
