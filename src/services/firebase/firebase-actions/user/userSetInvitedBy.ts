import firebase from 'react-native-firebase'

/**
 * 
 * @param uid current userid
 * @param invitedBy ref user who invited
 */
export const userSetInvitedBy = async (uid: string, invitedBy: string) => {
    try {
        const docRef = firebase.firestore().collection('users').doc(uid)
        const doc = await docRef.get()
        if (!doc.exists)
            return { ok: false, error: `users doc ${uid} doen't exist` }

        await docRef.set({ invitedBy: invitedBy }, { merge: true })
        return {
            ok: true,
            kind: 'set-user-invited-by',
            invitedBy
        }
    } catch (e) {
        console.tron.display({
            name: 'userSetInvitedBy firebase error - outer',
            preview: e.message,
            value: e,
            important: true
        })
        return { ok: false, error: 'unknown-error' }
    }
}
