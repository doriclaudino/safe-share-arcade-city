import { getRoot } from 'mobx-state-tree'

// for FCM
import firebase from 'react-native-firebase'

export const afterLogin = async (self, uid) => {

  const root: any = getRoot(self)

  const { chatStore, guildStore }: any = root
  // chatStore.joinPublicChatrooms()
  guildStore.listenForGuildInvites()

  setTimeout(() => { // Remind me why we do this
    chatStore.joinTwoPlayerChatrooms()
  }, 1000)


  // console.tron.log('Now lets try setting up firebase notifications')
  const FCM = firebase.messaging()
  const ref = firebase.firestore().collection('users')
  FCM.requestPermission()
  FCM.getToken().then(token => {
    // console.tron.log('looking for user w uid ' + uid)
    ref.doc(uid).set({ pushToken: token }, { merge: true })
    console.tron.log('Updated user with push token ' + token)
  })  
}
