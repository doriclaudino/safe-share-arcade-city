import * as React from 'react'
import { Animated, TouchableOpacity } from 'react-native'
import { textPresets, viewPresets } from './Button.presets'
import { Icon, Text } from '../'

const DEFAULT_ACTIVE_OPACITY: number = 0.6

interface ButtonProps {
  children?: any
  text?: string
}

interface ButtonState {
  bgAnim: any
}

export class Button extends React.PureComponent<ButtonProps, ButtonState> {
  constructor(props) {
    super(props)

    this.state = {
      bgAnim: new Animated.Value(0)
    }
  }

  componentDidMount() {
    if (this.props.animated) {
      setTimeout(this.fade.bind(this), this.props.fadeStart)
    }
  }

  fade() {
    Animated.timing(this.state.bgAnim, {
      duration: 1500,
      toValue: 1
    }).start()
  }

  public render() {
    const {
      preset = 'primary',
      animated,
      fadeStart = 0,
      children,
      text,
      icon,
      style: styleOverride,
      textStyle: textStyleOverride,
      ...rest
    } = this.props

    // grab the appropriate preset stuff to use
    const viewPresetToUse = viewPresets[preset] || viewPresets.primary
    const textPresetToUse = textPresets[preset] || textPresets.primary

    // assemble the base TouchableOpacity style
    const setViewStyle = {
      ...icon ? viewPresets.icon : {},
      ...viewPresetToUse,
      ...styleOverride
    }

    // assemble the base text style
    const setTextStyle = {
      ...textPresetToUse,
      ...icon ? textPresets.icon : {},
      ...textStyleOverride
    }

    if (animated && icon) {
      const animStyle = {
        opacity: this.state.bgAnim.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1]
        })
      }

      return (
        <Animated.View style={animStyle}>
          <TouchableOpacity {...rest} style={setViewStyle}>
            <Icon icon={icon} />
          </TouchableOpacity>
        </Animated.View>
      )
    }

    if (icon) {
      return (
        <TouchableOpacity {...rest} style={setViewStyle}>
          <Icon icon={icon} />
        </TouchableOpacity>
      )
    }

    return (
      <TouchableOpacity {...rest} style={setViewStyle}>
        <Text preset="label" text={text} style={setTextStyle} />
      </TouchableOpacity>
    )
  }
}
