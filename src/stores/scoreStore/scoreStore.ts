import { types } from 'mobx-state-tree'

const Score = types.model({
  id: types.identifier,
  city: types.string,
  tickets: types.number
})

/**
 * Handles scores state
 */
export const ScoreStoreModel = types
  .model('ScoreStore')
  .props({
    /** The scores */
    scores: types.optional(types.map(Score), {}),
  })

/**
 * An instance of a NavStore.
 */
export type ScoreStore = typeof ScoreStoreModel.Type

/**
 * The serialized version of a `ScoreStore` often used when acquiring
 * data from an API (for example).
 */
export type ScoreStoreSnapshot = typeof ScoreStoreModel.SnapshotType
