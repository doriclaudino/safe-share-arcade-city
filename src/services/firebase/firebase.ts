import firebase, { RNFirebase } from 'react-native-firebase'
import * as actions from './firebase-actions'
import { Chatroom, Coords, Guild, GuildMember, Player } from '../../stores'
import { GeoFirestore } from 'geofirestore'

/**
 * Define all the possible ways out of your service here.
 */
export type FirebaseServiceResponse =
  {
    ok: true
    kind: 'set-user-invited-by'
    invitedBy: string
  }
  | {
    ok: true
    kind: 'login'
    uid: string
    name: string
    fbid: string
    photoURL?: string
    email?: string
  }
  | {
    ok: true
    kind: 'fetch-user'
    bio: string
    role: string
    settings: any
    username: string
  }
  | {
    ok: true
    kind: 'fetch-public-chatrooms'
    chatrooms: Chatroom[]
  }
  | { ok: boolean, kind: string }
  | { ok: boolean, error: string }
  | { ok: boolean, kind: string, id: string }


/**
 * Responsible for talking with Firebase.
 */
export class Firebase {
  instance: any
  auth: RNFirebase.auth.Auth
  geofirePlayersRef: GeoFirestore
  geofireGuildsRef: GeoFirestore

  /** Will be called before any React components are initialized. */
  async setup() {
    this.instance = firebase as any
    this.auth = this.instance.auth()
    this.geofirePlayersRef = new GeoFirestore(this.instance.firestore().collection('geofire_players'))
    this.geofireGuildsRef = new GeoFirestore(this.instance.firestore().collection('geofire_guilds'))
  }


  /**
   * Fetch player data from firestore
   */
  async fetchGlobalGuilds() {
    return await actions.fetchGlobalGuilds()
  }


  async setTermsAgree(uid, when) {
    return await firebase
      .firestore()
      .collection('users')
      .doc(uid)
      .set({ agreedToTerms: when }, { merge: true })
      .then(res => {
        return { ok: true, kind: 'agreedToTerms' }
      })
      .catch(e => {
        return { ok: false, error: 'unknown-error' }
      })

  }



  /** Helper functions */
  geofireLocFromCoords(coords: Coords) {
    return {
      coordinates: new this.instance.firestore.GeoPoint(coords.lat, coords.lon)
    }
  }

  geopointFromCoords(coords: Coords) {
    return new this.instance.firestore.GeoPoint(coords.lat, coords.lon)
  }

  async beaconAdd(player: Player) {
    const location = this.geofireLocFromCoords(player.coords)
    delete player.coords
    this.geofirePlayersRef.set(player.uid, {
      ...location,
      ...player
    })
  }

  async beaconGuildAdd(id: string, coords: Coords) {
    const location = this.geofireLocFromCoords(coords)
    this.geofireGuildsRef.set(id, location)
  }


  async guildSetMission(id: string, mission: string) {
    return await actions.guildSetMission(id, mission)
  }

  async beaconRemove(id: string) {
    this.geofirePlayersRef.remove(id)
  }

  /**
   * Add user to guild members
   */
  async addMemberToGuild(member: GuildMember, guildId: string) {
    return await actions.addMemberToGuild(member, guildId)
  }

  /**
   * Fetch player data from firestore
   */
  async fetchPlayer(uid: string) {
    return await actions.fetchPlayer(uid)
  }

  /**
   * Remove user from guild members
   */
  async setDatabasePlayer(player: Player) {
    return await actions.setDatabasePlayer(player)
  }


  /**
   * Remove user from guild members
   */
  async removeMemberFromGuild(uid: string, guildId: string) {
    return await actions.removeMemberFromGuild(uid, guildId)
  }


  /**
   * Create chatroom for guild
   */
  async createGuildChatroom(guildId: string) {
    return await actions.createGuildChatroom(guildId)
  }


  /**
   * Create a guild.
   */
  async guildCreate(guild: Guild, founder: GuildMember): Promise<FirebaseServiceResponse> {

    // Create the guild in firestore 'guilds' collection
    const firebaseGuild = await actions.guildCreate(guild, founder)

    // Use the id to set the guild beacon
    const { id }: any = firebaseGuild
    const location = this.geofireLocFromCoords(guild.centerCoords)
    this.geofireGuildsRef.set(id, {
      ...location,
      ...guild
    })

    return { ok: true, kind: 'guild-create', id }
  }

  // resetScore(): Promise<void> {
  //   return this.usersCollectionRef.ref.get().then(resp => {
  //     console.log(resp.docs)
  //     let batch = this.afs.firestore.batch();
  //
  //     resp.docs.forEach(userDocRef => {
  //       batch.update(userDocRef.ref, {'score': 0, 'leadsWithSalesWin': 0, 'leadsReported': 0});
  //     })
  //     batch.commit().catch(err => console.error(err));
  //   }).catch(error => console.error(error))
  // }

  async ignoreInvitesFromGuild(uid: string, guildId: string): Promise<FirebaseServiceResponse> {

    await firebase
      .firestore()
      .collection('guild_invites')
      .doc(uid)
      .collection('invites')
      .get()
      .then(resp => {
        console.tron.log('heyyyy:', resp.docs)
        let batch = firebase.firestore().batch()

        resp.docs.forEach(inviteDocRef => {
          console.tron.log(inviteDocRef.ref, inviteDocRef)
          batch.update(inviteDocRef.ref, { status: 'ignored' })
        })
        batch.commit().catch(err => console.error(err))
      })
      .catch(e => console.tron.log(e.message))

    return { ok: true, kind: 'yar' }
  }




  /**
   * Update a user obj and beacon with guild id-- used after user creates or joins a guild
   */
  async updateUserWithGuildId(uid: string, guildId: string): Promise<FirebaseServiceResponse> {

    const userRef = firebase.firestore().collection('geofire_players').doc(uid)

    await userRef.get()
      .then((docSnapshot) => {
        if (docSnapshot.exists) {
          userRef.set({ d: { guild: guildId } }, { merge: true })
        }
        return true
      })

    return await firebase
      .firestore()
      .collection('players')
      .doc(uid)
      .set({ guild: guildId }, { merge: true })
      .then(res => {
        return { ok: true, kind: 'updateUserWithGuildId' }
      })
      .catch(e => {
        return { ok: false, error: 'unknown-error' }
      })

  }


  /**
   * f
   */
  async fetchGuildChatroom(guildId: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.fetchGuildChatroom(guildId)
  }


  /**
   * Attempt to fetch array of all public chatrooms from Firestore.
   */
  async fetchPublicChatrooms(): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.fetchPublicChatrooms()
  }


  /**
   * Attempt to fetch array of all two-player chatrooms from Firestore
   * which current user belongs to.
   */
  async fetchTwoPlayerChatrooms(self: any, uid: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.fetchTwoPlayerChatrooms(self, uid)
  }


  /**
   * Attempt to fetch user data from Firestore.
   */
  async userFetchData(uid: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.userFetchData(uid)
  }


  /**
   * Attempt to fetch guild members from Firestore.
   */
  async fetchGuildMembers(uid: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.fetchGuildMembers(uid)
  }


  /**
   * Listen for GUILD INVITES
   */
  async listenForGuildInvites(self: any, uid: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.listenForGuildInvites(self, uid)
  }


  /**
   * INVITE TO GUILD!!!!!!111111
   */
  async guildInviteMember(guildId: string, uid: string, myid: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.guildInviteMember(guildId, uid, myid)
  }


  /**
   * Attempt to log in with Facebook.
   */
  async loginWithFacebook(token: string): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return await actions.loginWithFacebook(this, token)
  }


  /**
   * Attempt to logout
   */
  async logOut(): Promise<FirebaseServiceResponse> {
    return await actions.logOut(this)
  }


  /**
   * Attempt to send message to chatroom.
   */
  async messageSend({ text, roomId, userId, userName, userPhoto }): Promise<FirebaseServiceResponse> {
    return await actions.messageSend({ text, roomId, userId, userName, userPhoto })
  }


  /**
   * Set the user bio
   */
  async userSetBio(uid: string, bio: string): Promise<FirebaseServiceResponse> {
    return await actions.userSetBio(uid, bio)
  }


  /**
   * Set the user role
   */
  async userSetRole(uid: string, role: string): Promise<FirebaseServiceResponse> {
    return await actions.userSetRole(uid, role)
  }


  /**
   * Set the user username
   */
  async userSetUsername(uid: string, username: string): Promise<FirebaseServiceResponse> {
    return await actions.userSetUsername(uid, username)
  }


  /**
   * Set push notification enable disable on login and logout application
   */
  async userSetNotification(uid: string, isNotification): Promise<FirebaseServiceResponse> {
    return await actions.userSetNotification(uid, isNotification)
  }


  /**
  * Set invitedBy user
  */
  userSetInvitedBy(uid: string, invitedBy: string): Promise<FirebaseServiceResponse> {
    return actions.userSetInvitedBy(uid, invitedBy)
  }


  /**
   * Set chatroom setting
   */
  async saveSettings(uid: string, settings: any): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    console.tron.log('trying w uid', settings)
    // @ts-ignore
    return firebase
      .firestore()
      .collection('users')
      .doc(uid)
      .set({ settings }, { merge: true })
      .then(res => {
        // console.tron.log('yay!', res)
        return { ok: true, kind: 'save-settings' }
      })
      .catch(e => {
        return { ok: false, error: 'unknown-error' }
      })

  }


  /**
   * Attempt to send message to chatroom.
   */
  async deleteMessage(id): Promise<FirebaseServiceResponse> {
    // @ts-ignore
    return firebase
      .firestore()
      .collection('messages')
      .doc(id)
      .delete()
      .then(res => {
        console.tron.log('deleted')
        return { ok: true, kind: 'delete-message' }
      })
      .catch(e => {
        return { ok: false, error: 'unknown-error' }
      })
  }

}
