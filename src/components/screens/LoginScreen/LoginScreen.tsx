import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { UserStore } from '../../stores'
import { Background, Button, Container, Text } from '../../'

export interface LoginScreenProps {
  userStore?: UserStore
}

@inject('userStore')
@observer
export class LoginScreen extends React.Component<LoginScreenProps, {}> {
  public render() {
    const { login } = this.props.userStore
    return (
      <Background>
        <Container preset="loginSplash">
          <Container preset="loginButtons">
            <Button
              preset="socialLogin"
              onPress={() => login('facebook')}
              icon="facebook"
              fadeStart={1500}
              animated
            />
            <Button
              preset="socialLogin"
              onPress={() => login('twitter')}
              icon="twitter"
              fadeStart={2000}
              animated
            />
            <Button
              preset="socialLogin"
              onPress={() => login('google')}
              icon="google"
              fadeStart={2500}
              animated
            />
            <Button
              preset="socialLogin"
              onPress={() => login('github')}
              icon="github"
              fadeStart={3000}
              animated
            />
          </Container>
        </Container>
      </Background>
    )
  }
}


//
// <Text
//   preset="epicSubtitle"
//   text="the"
//   fadeStart={800}
//   animated
// />
// <Text
//   preset="epicTitle"
//   text="CITY"
//   fadeStart={1500}
//   animated
// />
