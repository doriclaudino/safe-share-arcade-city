import * as React from 'react'
import { createStackNavigator } from 'react-navigation'
import { MainMenu } from '../../../../views/menu'
import { color, typography } from '../../../../views/theme'
import { NavButton } from '../../../../views/shared'
import { Settings, Selector } from '../../../../views/comms';
import { CreatePasswordScreen } from '../../../../views/comms/screens/create-password';

export const MenuNavigator = createStackNavigator(
  {
    menu: { screen: MainMenu, navigationOptions: { header: null } },
    settings: { screen: Settings , navigationOptions: { header: null }},
    selector: { screen: Selector , navigationOptions: { header: null }},
    createPassword: { screen: CreatePasswordScreen , navigationOptions: { header: null }},
  },
  {
    headerMode: 'screen',
    initialRouteName: 'menu',
    navigationOptions: ({ navigation }: any) => ({
      gesturesEnabled: false,
      headerTintColor: color.palette.white,
      headerStyle: {
        backgroundColor: color.background,
        borderBottomColor: color.background,
        elevation: 0
      },
      headerLeft: <NavButton onPress={() => navigation.goBack() /** ?? */} />,
      headerTitleStyle: {  // ?
        fontFamily: typography.bold
      }
    })
  }
)
