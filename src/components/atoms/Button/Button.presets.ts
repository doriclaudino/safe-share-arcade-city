import { TextStyle, ViewStyle } from 'react-native'

const BASE_VIEW: ViewStyle = {
  paddingVertical: 4,
  paddingHorizontal: 4,
  borderRadius: 4,
  alignSelf: 'stretch',
  minHeight: 50,
  minWidth: 120,
  alignItems: 'center',
  justifyContent: 'center',
  shadowOffset: {
    width: 0,
    height: 6
  },
  shadowOpacity: 1,
  shadowRadius: 12
}

const BASE_TEXT: TextStyle = {
  color: '#fff',
  textAlign: 'center',
  paddingHorizontal: 8
}

/**
 * What the base view looks like.
 */
export const viewPresets = {
  primary: {
    ...BASE_VIEW,
    backgroundColor: 'blue',
    shadowColor: 'rgba(91, 32, 242, 0.2)'
  } as ViewStyle,
  socialLogin: {
    ...BASE_VIEW,
    backgroundColor: 'rgba(25, 5, 41, 0.7)',
    minHeight: 90,
    minWidth: 90,
    height: 100,
    width: 100,
    borderRadius: 15,
    margin: 18
  }
}

/**
 * What the text looks like.
 */
export const textPresets = {
  primary: {
    ...BASE_TEXT
  } as TextStyle,
  socialLogin: {
    ...BASE_TEXT
  } as TextStyle
}

/**
 * A list of preset names.
 */
export type ButtonPresetNames = keyof typeof viewPresets
