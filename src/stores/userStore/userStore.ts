import { types } from 'mobx-state-tree'
import * as actions from './userActions'

const FeedEvent = types.model({
  id: types.identifier,
  name: types.string
})

const Profile = types.model({
  username: types.maybe(types.string)
})

const Settings = types.model({
  everything: true
})

/**
 * Handles user state
 */
export const UserStoreModel = types
  .model('UserStore')
  .props({
    /** The event feed visible to current user */
    feed: types.optional(types.map(FeedEvent), {}),
    /** The current user's profile */
    profile: types.optional(Profile, {}),
    /** Settings for current user */
    settings: types.optional(Settings, {}),
    /** Number of tickets current user has */
    tickets: types.optional(types.number, 0)
  })
  .actions(self => ({
    /** Login user */
    login: async (method: string): Promise<any> =>
      await actions.login(self, method),
    /** Basic setters */
    setProfile (value: Profile) {
      self.profile = value
    }
  }))

/**
 * An instance of a UserStore.
 */
export type UserStore = typeof UserStoreModel.Type

/**
 * The serialized version of a `UserStore` often used when acquiring
 * data from an API (for example).
 */
export type UserStoreSnapshot = typeof UserStoreModel.SnapshotType
