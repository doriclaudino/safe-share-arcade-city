import * as React from 'react'
import { Animated, Text as RNText } from 'react-native'
import { presets } from './Text.presets'

interface TextProps {
  text: string
}

interface TextState {
  bgAnim: any
}

export class Text extends React.PureComponent<TextProps, TextState> {
  constructor(props) {
    super(props)

    this.state = {
      bgAnim: new Animated.Value(0)
    }
  }

  componentDidMount() {
    if (this.props.animated) {
      setTimeout(this.fade.bind(this), this.props.fadeStart)
    }
  }

  fade() {
    Animated.timing(this.state.bgAnim, {
      duration: 1500,
      toValue: 1
    }).start()
  }

  public render() {
    const {
      preset = 'default',
      text,
      children,
      style: styleOverride,
      animated,
      ...rest
    } = this.props

    const content = text || children
    const presetToUse = presets[preset] || presets.default
    const style = { ...presetToUse, ...styleOverride }

    if (animated) {
      const animStyle = {
        opacity: this.state.bgAnim.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1]
        })
      }

      return (
        <Animated.View style={animStyle}>
          <RNText {...rest} style={style}>
            {content}
          </RNText>
        </Animated.View>
      )
    }

    return (
      <RNText {...rest} style={style}>
        {content}
      </RNText>
    )
  }

}
