import { types } from 'mobx-state-tree'
import { NavStoreModel } from '../navStore'
import { UserStoreModel } from '../userStore'

/**
 * A RootStore model.
 */
export const RootStoreModel = types
  .model('RootStore')
  .props({
    navStore: types.optional(NavStoreModel, {}),
    userStore: types.optional(UserStoreModel, {})
  })

/**
 * The RootStore instance.
 */
export type RootStore = typeof RootStoreModel.Type

/**
 * The data of an RootStore.
 */
export type RootStoreSnapshot = typeof RootStoreModel.SnapshotType
