import { ViewStyle } from 'react-native'

/** All containers will start off looking like this. */
const BASE: ViewStyle = {
  backgroundColor: 'transparent',
  alignItems: 'center',
  justifyContent: 'center'
}

/** All the variations of container styling within the app. */
export const presets = {
  /** The default container styles. */
  default: BASE,

  /** The login splash container */
  loginSplash: {
    ...BASE,
    flex: 1,
    paddingTop: 40
  },

  /** Container for login buttons on splash screen */
  loginButtons: {
    ...BASE,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: 300
  } as ViewStyle,
}

/** A list of preset names. */
export type ContainerPresetNames = keyof typeof presets
