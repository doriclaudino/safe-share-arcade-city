import { TextStyle } from 'react-native'
// import { color, spacing, typography } from '../../../theme'

/**
 * All text will start off looking like this.
 */
const BASE: TextStyle = {
  fontFamily: 'Courier' , // typography.primary
  color: '#fff',
  fontSize: 14,
  lineHeight: 22
}

/**
 * All the variations of text styling within the app.
 */
export const presets = {
  /**
   * The default text styles.
   */
  default: BASE,

  /**
   * A bold version of the default text.
   */
  epicTitle: {
    ...BASE,
    fontSize: 44,
    lineHeight: 48,
    letterSpacing: 12
  } as TextStyle,

  /**
   * A bold version of the default text.
   */
  epicSubtitle: {
    ...BASE,
    fontSize: 24,
    lineHeight: 48,
    letterSpacing: 3
  } as TextStyle,

  /**
   * A bold version of the default text.
   */
  bold: { ...BASE, fontWeight: 'bold' } as TextStyle,

  /**
   * Labels that appear on forms above the inputs or on buttons.
   */
  label: { ...BASE, lineHeight: 16, fontWeight: 'bold' } as TextStyle, //
}

/**
 * A list of preset names.
 */
export type TextPresetNames = keyof typeof presets
